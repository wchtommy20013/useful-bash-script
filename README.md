#genrandsha256
###Use: genrandsha256 -> Generate a random string with length of 64
require: sha256sum

#swap
###Use: swap [/path/to/file/1] [/path/to/file/2] -> Swap file 1 and 2\n
require: sha256sum

#ckextip 
###Use: ckextip [email address] -> Check External IP and Send It to [email address] if it changes\n
require: CentOS, curl, mailx




